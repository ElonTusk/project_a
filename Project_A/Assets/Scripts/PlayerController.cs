﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed = 5.0f;
    public Joystick joystic;
    private float horizontalMove = 0f;
    
    void Update()
    {
        if (joystic.Horizontal >= .2f)
        {
            rb.AddForce(joystic.Horizontal * speed * Vector2.right);
        }
        else if (joystic.Horizontal <= .2f)
        {
            rb.AddForce(joystic.Horizontal * speed * Vector2.right);
        }
        else
        {
            horizontalMove = 0f;
        }
    }
}
