﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Rigidbody2D rb;
    public Joystick joystick;
    public Button jumpBtn;
    
    private void FixedUpdate()
    {
        if (joystick.Horizontal > 0)
        {
            rb.velocity = new Vector2(2, rb.velocity.y);
        }
        else if (joystick.Horizontal < 0)
        {
             rb.velocity = new Vector2(-2, rb.velocity.y);
        }
        
        jumpBtn.onClick.AddListener(Jump);
    }

    private void Jump()
    {
        rb.position = new Vector2(rb.position.x, -1);
    }
}
